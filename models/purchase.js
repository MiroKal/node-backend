const mongoose = require('mongoose')

const purchaseSchema = new mongoose.Schema({
    purchaseDate: {
        type: Date
    },
    userName: {
        type: String,
        required: true,
        ref: 'User'
    },
    productId: {
        type: Number,
        required: true,
        ref: 'Purchase'
    },
    quantity: {
        type: Number,
        required: true,
        min: 0
    }
})

const Purchase = mongoose.model("Purchase", purchaseSchema);
module.exports = Purchase;