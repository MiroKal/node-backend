const mongoose = require('mongoose')
const validator = require('validator')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const Purchase = require('./purchase')

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error("Enter valid email.")
            }
        }
    },
    status: {
        type: String,
        required: true,
        trim: true,
        uppercase: true,
        enum: ["ACTIVE", "INACTIVE"],
    },
    password: {
        type: String,
        required: true,
        minlength: [7, "Password should be longer than 7 characters"],
        trim: true
    },
    userName: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    __v: {
        type: Number
    },
    roles: [{
        role: {
            required: true,
            type: String,
            uppercase: true,
            trim: true,
            enum: ["ADMIN", "USER"]
        }
    }]
})

userSchema.methods.generateAuthToken = async function () {
    const user = this
    const token = jwt.sign({ _id: user._id.toString(), roles: user.roles }, process.env.JWT_SECRET)

    return token
}

userSchema.statics.findByCredentials = async (userName, password) => {
    const user = await User.findOne({ userName })

    if (!user) {
        throw new Error('Unable to login')
    }

    const isMatch = await bcrypt.compare(password, user.password)

    if (!isMatch) {
        throw new Error('Unable to login')
    }

    return user
}

userSchema.pre('save', async function (next) {
    const user = this

    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }

    next()
})

const User = mongoose.model("User", userSchema);
module.exports = User;