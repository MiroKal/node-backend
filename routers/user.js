const express = require('express')
const User = require('../models/user')
const router = new express.Router()

router.post("/user", async (req, res) => {
    const user = new User({
        "userName": req.body.userName,
        "roles": [{ "role": req.body.roles }],
        "password": req.body.password,
        "status": req.body.status,
        "email": req.body.email
    })

    try {
        await user.save();
        res.status(201).send({
            "data": {
                "message": "User succesfully created."
            }
        }
        )
    } catch (e) {
        res.status(400).send(e.message);
    }
})

router.get("/user", async (req, res) => {
    let pageNum = 0;
    let perPage = 10;
    if (req.query.pageNum) {
        pageNum = Number(req.query.pageNum);
    }
    if (req.query.perPage) {
        perPage = Number(req.query.perPage);
    }
    delete req.query.pageNum;
    delete req.query.perPage;
    try {
        const users = await User.find(
            { userName: req.query.userName },
            undefined,
            { skip: pageNum, limit: perPage }
        );
        if (users.length == 0) {
            res.status(404).send("Users not found.");
        } else {
            res.status(200).send(users);
        }
    } catch {
        res.status(400).send();
    }
})

router.put("/user/:userName", async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['email', 'roles', 'status']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        return res.status(400).send({ error: 'Invalid update fields!' })
    }

    try {
        await User.findOneAndUpdate({ userName: req.params.userName }, {
            "roles": [{ "role": req.body.roles }],
            "status": req.body.status,
            "email": req.body.email
        })
        res.send({
            "data": {
                "message": "User succesfully updated."
            }
        })
    } catch (e) {
        res.status(400).send(e.message)
    }
})

router.delete("/user/:userName", async (req, res) => {
    try {
        await User.findOneAndDelete({ userName: req.params.userName })
        res.send({
            "data": {
                "message": "User succesfully deleted."
            }
        })
    } catch (e) {
        res.status(400).send(e.message)
    }
})

module.exports = router;