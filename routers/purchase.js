const express = require('express')
const Purchase = require('../models/purchase')
const router = new express.Router()

router.get("/purchase", async (req, res) => {
    let pageNum = 0;
    let perPage = 10;
    if (req.query.productId) {
        req.query.productId = Number(req.query.productId);
    }
    if (req.query.pageNum) {
        pageNum = Number(req.query.pageNum);
    }
    if (req.query.perPage) {
        perPage = Number(req.query.perPage);
    }
    try {
        const purchases = await Purchase.find({
            userName: req.query.userName,
            purchaseDate: {
                $gte: req.query.dateFrom,
                $lte: req.query.dateTo
            },
            productId: req.query.productId
        }, null)
            .skip(pageNum)
            .limit(perPage);
        if (purchases.length == 0) {
            res.status(404).send({ data: "Purchase is not found" });
        } else {
            res.send({ data: purchases });
        }
    } catch (error) {
        res.status(400).send(error);
    }
})

router.post("/purchase", async (req, res) => {
    const purchase = new Purchase(req.body)

    try {
        await purchase.save();
        res.status(201).send({
            "data": {
                "message": "Purchase succesfully created."
            }
        }
        )
    } catch (e) {
        res.status(400).send(e.message);
    }
})

module.exports = router;