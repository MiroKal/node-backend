const express = require('express')
const User = require('../models/user')
const router = new express.Router()

router.post("/auth/authenticateuser", async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.userName, req.body.password)

        if (!user) {
            throw new Error()
        }

        const token = await user.generateAuthToken();
        res.status(200).send({
            "data": {
                "token": token,
                "message": "Token is succesfully created",
                "code": 200
            }
        })
    } catch (e) {
        res.status(403).send(e.message)
    }
})

module.exports = router;