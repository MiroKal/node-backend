const express = require('express')
const get = require("../db/mongodb")
const router = new express.Router()

router.post("/product", async (req, res) => {
    let db = get();
    let counter;
    try {
        const counterArray = await db.collection("productCounters").find().toArray();
        if (counterArray == 0) {
            await db.collection("productCounters").insertOne({
                "_id": "productCounterID",
                "productCounter": 0
            })
        }
        const counterAwait = await db.collection("productCounters").findOneAndUpdate({ "_id": "productCounterID" }, { $inc: { "productCounter": 1 } });
        counter = counterAwait.value.productCounter;
    } catch (e) {
        res.send(e.message)
    }
    try {
        await db.collection("product").insertOne({
            _id: counter,
            ...req.body
        })
        res.status(200).send({
            "data": {
                "message": "Product successfully stored into database."
            },
            "resource": [
                {
                    ...req.body,
                    "_id": counter
                }
            ]
        });
    } catch (e) {
        res.status(400).send(e.message);
    }
})

router.get("/product", async (req, res) => {
    let db = get();
    let pageNum = 0;
    let perPage = 8;
    if (req.query.id) {
        req.query._id = Number(req.query.id);
        delete req.query.id;
    }
    if (req.query.category) {
        req.query.category = Number(req.query.category);
    }
    if (req.query.pageNum) {
        pageNum = Number(req.query.pageNum);
    }
    if (req.query.perPage) {
        perPage = Number(req.query.perPage);
    }
    delete req.query.pageNum;
    delete req.query.perPage;

    try {
        const cursorForCount = await db.collection("product").aggregate([
            {
                $match: req.query
            },
            {
                $count: "product_count"
            }
        ]);
        const resultForCount = await cursorForCount.toArray();

        const cursorForProducts = await db.collection("product").aggregate([
            {
                $match: req.query
            },
            {
                $lookup: {
                    from: "category",
                    localField: "category",
                    foreignField: "_id",
                    as: "category"
                }
            },
            {
                $skip: pageNum * perPage
            },
            {
                $limit: perPage
            }
        ]);
        const resultForProducts = await cursorForProducts.toArray();
        res.status(200).send({
            products: resultForProducts,
            count: resultForCount[0] ? resultForCount[0].product_count : 0
        });
    } catch (e) {
        res.status(400).send(e.message);
    }
});

router.put("/product/:id", async (req, res) => {
    let db = get();
    try {
        await db.collection("product").findOneAndUpdate({ _id: Number(req.params.id) }, { $set: req.body }, (error, success) => {
            if (error) {
                res.status(400).send(error);
            }
            if (success) {
                res.status(200).send({
                    "data": {
                        "message": "Product successfully updated in database."
                    }
                });
            }
        });
    }
    catch (e) {
        res.status(400).send(e.message);
    }
})

router.delete("/product/:id", async (req, res) => {
    let db = get();
    try {
        await db.collection("product").findOneAndDelete({ _id: Number(req.params.id) });
        res.status(204).send();
    } catch (e) {
        res.status(400).send(e.message);
    }
})

module.exports = router;