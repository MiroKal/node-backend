const express = require('express')
const get = require("../db/mongodb")
const router = new express.Router()

router.get("/category", async (req, res) => {
    let db = get();
    if (req.query.name) {
        try {
            await db.collection("category").findOne({ "name": req.query.name }, undefined, (error, success) => {
                if (error) {
                    res.status(404).send(error);
                }
                if (success) {
                    res.status(200).send(success);
                }
            });
        } catch (e) {
            res.status(404).send(e.message);
        }
    } else {
        try {
            await db.collection("category").find({})
                .toArray((error, success) => {
                    if (error) {
                        res.status(404).send(error);
                    }
                    if (success) {
                        res.status(200).send(success);
                    }
                });
        } catch (e) {
            res.status(404).send(e.message);
        }
    }
});

router.post("/category", async (req, res) => {
    let db = get();
    let counter;
    try {
        const counterArray = await db.collection("categoryCounters").find().toArray();
        if (counterArray == 0) {
            await db.collection("categoryCounters").insertOne({
                "_id": "categoryCounterID",
                "categoryCounter": Number(0)
            })
        }
        const counterAwait = await db.collection("categoryCounters").findOneAndUpdate({ "_id": "categoryCounterID" }, { $inc: { "categoryCounter": 1 } });
        counter = counterAwait.value.categoryCounter;
    } catch (e) {
        res.send(e.message)
    }
    try {
        await db.collection("category").insertOne({
            _id: counter,
            ...req.body
        })
        res.status(200).send({
            "data": {
                "message": "Category successfully stored into database."
            },
            "resource": [
                {
                    ...req.body,
                    "_id": counter
                }
            ]
        });
    } catch (e) {
        res.status(400).send(e.message);
    }
});

router.delete("/category/:id", async (req, res) => {
    let db = get();
    try {
        await db.collection("category").findOneAndDelete({ _id: Number(req.params.id) });
        res.status(204).send();
    } catch (e) {
        res.status(400).send(e.message);
    }
})

module.exports = router;