const mongodb = require("mongodb");
const MongoClient = mongodb.MongoClient;
let db;

MongoClient.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true
}, async (error, client) => {
    if (error) {
        console.log("Unable to connect to database!");
    }
    db = await client.db("masterclass-database");

})

const get = function () {
    return db;
}

module.exports = get;
