const express = require('express')
const cors = require('cors');
require('./db/mongoose')
const userRouter = require('./routers/user')
const categoryRouter = require('./routers/category')
const loginRouter = require('./routers/login')
const productRouter = require('./routers/product')
const purchaseRouter = require('./routers/purchase')


const app = express()

app.use(cors());
app.use(express.json())
app.use(userRouter)
app.use(categoryRouter)
app.use(loginRouter)
app.use(productRouter)
app.use(purchaseRouter)

module.exports = app